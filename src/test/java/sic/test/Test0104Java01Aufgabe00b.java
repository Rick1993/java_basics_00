/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import sic.test.util.SimpleJunit5Launcher;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01.
 * <p>
 * Basic Java expressions and statements, part 0b (branches and loops)
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test0104Java01Aufgabe00b {
    @Test
    void test00() {
        // dummy
    }

    @SuppressWarnings("unused")
    // @Test
    void test20_branch() {
        // branches: if
        {
            {
                final int a = 8; // FIXME
                final int b = 8; // FIXME
                boolean c;
                if( a < b ) {  // don't touch
                    c = true;  // don't touch
                } else {
                    c = false; // don't touch
                }
                Assertions.assertTrue(c); // don't touch
            }
            {
                final int a = 2; // don't touch
                final int b = 1; // don't touch
                boolean c;
                if( a < b ) {  // FIXME
                    c = true;  // don't touch
                } else {
                    c = false; // don't touch
                }
                Assertions.assertTrue(c); // don't touch
            }
            {
                final int a = 8; // FIXME
                final int b = 8; // FIXME
                final boolean c = a > b; // don't touch
                Assertions.assertTrue(c); // don't touch
            }
            {
                final int a = -2; // don't touch
                final int b =  2; // don't touch
                final boolean c = a > b; // FIXME
                Assertions.assertTrue(c); // don't touch
            }
        }

        // branches: switch
        {
            int state = -1;  // don't touch
            final int a = 8; // don't touch

            switch( a ) {    // don't touch
                case 0:
                    state = 8; // FIXME?
                    break;
                case 1:
                    state = 8; // FIXME?
                    break;
                case 2:
                    state = 8; // FIXME?
                    break;
                default:
                    state = 8; // FIXME?
                    break;
            }
            Assertions.assertEquals(1, state); // don't touch
        }

        // branches: conditional operator
        {
            {
                final int a = 8; // don't touch

                final char c = ( 9 < a ) ? '0' : '1'; // FIXME?

                Assertions.assertEquals('0', c); // don't touch
            }
            {
                final int a = 2; // don't touch
                final int b = 7; // FIXME?

                // chained conditional operator, sort of bad style but valid
                final char c = ( 1 == a ) ? ( 7 > b ? '0' : '2' ) : '1'; // FIXME?

                Assertions.assertEquals('0', c); // don't touch
            }
        }
    }

    // @Test
    void test21_loops() {
        //
        // Only touch lines with FIXME!
        //
        final int loop_count = 3; // don't touch

        // while loops, an exploded for-loop (see below)
        {
            {
                int v=5;  // FIXME
                int i=0;              /* instantiation and initialization of loop variable */
                while( i < loop_count /* while condition */ ) {
                    ++v; // FIXME?
                    i = i + 1;        /* tail expression */
                }
                Assertions.assertEquals(loop_count, i);
                Assertions.assertEquals(loop_count, v); // FIXME?
            }
            {
                int v=0;
                int i=0;              /* instantiation and initialization of loop variable */
                while( i < loop_count /* while condition */ ) {
                    --v;
                    i = i + 1;        /* tail expression */
                }
                Assertions.assertEquals(loop_count, i);
                Assertions.assertEquals(loop_count, v+i); // FIXME
            }
        }

        // do-while loops - executed at least once
        {
            {
                int v=-2; // FIXME?
                int i=0;              /* instantiation and initialization of loop variable */
                do {
                    ++v;
                    i = i + 1;        /* tail expression */
                } while( i < loop_count /* while condition */ );
                Assertions.assertEquals(loop_count, i);
                Assertions.assertEquals(loop_count, v+i); // FIXME?
            }
            {
                int v=loop_count;
                do {
                    v--;
                } while( v < loop_count /* while condition */ ); // FIXME
                Assertions.assertEquals(loop_count, v); // FIXME
            }
        }

        // for-loops
        {
            {
                int i;                /* instantiation of loop variable*/
                for(i=10              /* initialization of loop variable FIXME */;
                    0<i               /* while condition */;
                    ++i               /* tail expression FIXME */)
                { }
                // `i` is still in scope!
                Assertions.assertEquals(0, i); // don't touch
            }
            {
                int v=10; // FIXME
                /* instantiation and initialization of loop variable; while condition; tail expression */
                for(int i=0; i<loop_count; ++i) {
                    ++v; // FIXME?
                }
                // `i` is out of scope!
                // Assertions.assertEquals(loop_count, i);
                Assertions.assertEquals(loop_count, v);
            }
        }

        // Using break within a loop
        {
            int v=loop_count;
            int i=0;              /* instantiation and initialization of loop variable */
            while( true /* while condition: forever */ ) {
                if( i >= loop_count ) {
                    break; // exit loop
                }
                ++v; // FIXME
                i = i + 1;        /* tail expression */
            }
            Assertions.assertEquals(loop_count, i);
            Assertions.assertEquals(loop_count, v); // FIXME
        }
    }

    public static void main(final String[] args) {
        SimpleJunit5Launcher.runTests(Test0104Java01Aufgabe00b.class);
    }

}
