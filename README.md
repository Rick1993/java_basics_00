
# Java Basics 00

## Goals
This project is part of a basic computer science class using Java.

### Status
Build and test clean using Java 17.

## Building Binaries

### Build Dependencies
- Maven >= 3
- OpenJDK >= 17
- junit5 >= 5.5.2

### Build Procedure
- Run tests: `mvn test`
- Build package: `mvn package`

## IDE Integration

### Eclipse 
Tested Eclipse 2024-06 (4.32).

IDE integration configuration files are provided for 
- [Eclipse](https://download.eclipse.org/eclipse/downloads/) with extensions
  - Maven Plugin: `M2E - Maven Integration for Eclipse`

### VSCodium or VS Code

IDE integration configuration files are provided for 
- [VSCodium](https://vscodium.com/) or [VS Code](https://code.visualstudio.com/) with extensions
  - Java Support
    - [redhat.java](https://github.com/redhat-developer/vscode-java#readme)
      - Notable, `.settings/org.eclipse.jdt.core.prefs` describes the `lint` behavior
    - [vscjava.vscode-java-test](https://github.com/Microsoft/vscode-java-test)
    - [vscjava.vscode-java-debug](https://github.com/Microsoft/java-debug)
    - [vscjava.vscode-maven](https://github.com/Microsoft/vscode-maven/)
  - [cschlosser.doxdocgen](https://github.com/cschlosser/doxdocgen)
  - [jerrygoyal.shortcut-menu-bar](https://github.com/GorvGoyl/Shortcut-Menu-Bar-VSCode-Extension)

